## General
This container can be used to launch any executable binary or script.

### Sample usage
Prepare SCRIPT folder and sample shell script
```
mkdir -p $HOME/SCRIPTS
vi $HOME/SCRIPTS/test-bootstrap.sh
```
Sample shell script
```
#!/bin/bash
#======================================================
#======================================================
#                    test-bootstrap
#======================================================
#======================================================

echo  "[INFO] test-bootstrap executed"

# Keep running
sleep infinity

```
Save and make it executable
```
chmod +x $HOME/SCRIPTS/test-bootstrap.sh
```


Create sample test-bootstrap container

* BOOTSTRAP - Required environment variable contains full file path to the executable binary or script within container


```
docker run -tid \
            --name test-bootstrap \
            --hostname test-bootstrap \
            -e PUID=1000 \
            -e PGID=1000 \
            -e TZ="Europe/Bucharest" \
            -e BOOTSTRAP="/SCRIPTS/test-bootstrap.sh" \
            -v $HOME/SCRIPTS:/SCRIPTS \
            myset/bootstrap:latest
```
