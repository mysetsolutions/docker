#!/bin/bash
#===========================================================
#===========================================================
#              myset/bootstrap build utilities
#===========================================================
#===========================================================

# Login
docker login

# Check
docker image ls | grep myset/bootstrap

# Build
docker build --rm -t myset/bootstrap:latest .
docker build --rm -t myset/bootstrap:202306 .

# Push
docker push myset/bootstrap:latest
docker push myset/bootstrap:202306

# Clean
docker rm myset/bootstrap:latest
docker rm myset/bootstrap:202306

# Pull
docker pull myset/bootstrap:latest
docker pull myset/bootstrap:202306

# Test
docker container stop test-bootstrap
docker container rm test-bootstrap
docker run -tid \
            --name test-bootstrap \
            --hostname test-bootstrap \
            -e PUID=1000 \
            -e PGID=1000 \
            -e TZ="Europe/Bucharest" \
            -e BOOTSTRAP="/SCRIPTS/test-bootstrap.sh" \
            -v $HOME/SCRIPTS:/SCRIPTS \
            myset/bootstrap:latest