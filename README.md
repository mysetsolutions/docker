## About
This project is designed for building docker images. 

Each folder contains specific images and builds. Please check  [Docker Hub](https://hub.docker.com/orgs/myset/repositories) published images.

* **bootstrap** - Bootstrap any executable binary or script on Ubuntu 22.04 LTS image
* **samba** - Bootstrap any executable binary or script on **bootstrap** image with [samba](https://www.samba.org/)


## Builds
Builds contains labels **image.version**: 'last two digits' from year and **image.date**: build date  


