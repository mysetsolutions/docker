## General
Do you want to contribute your work? Well, then first and most important: **THANK YOU!**

## Who can contribute?
Anyone, with the unique condition that he/she must be a private individual, acting in his/her own name, and not being endorsed in their contributed work by any company or government.

Note that this condition will not only refer to the ownership of the effort invested in contributing to the project, but also to the fact that no private or public company will be mentioned as a a part of your contribution on the project's website or code, including but not limited to web/email addresses or package names.
What is the first step to be taken?

First of all, Discuss with the project members (a new thread should do) about your ideas: new features, fixes, documentation... whatever you would like to contribute to the project. Let we discuss the possibilities with you so that we make sure your contribution goes in the right direction and aligns with the project's standards, intentions and roadmap.

Your contributions will have the form of Git SCM pull requests. Note that contributors do not have read+write (or pull+push) access to the project repositories, only project members do.

Also, please understand that not all pull requests will be accepted and merged into the project's repositories. Talking about your planned contributions with the project members before creating pull requests will maximize the possibilities of your contributions being accepted.

## About the code you contribute

General guidelines:
* Obviously, your code must both compile and work correctly. Also, the addition of any new patches to the codebase should not render it unstable in any way.
* All your code should be easy to read and understand by a human.
* There should be no compilation warnings at all.
* Checkstyle must pass without errors or warnings.


## Pay special attention to this

All software is distributed under the [Apache License 2.0](LICENSE.txt) open source license, and your contributions will be licensed in the same way.

If you work for a company which, by the way or place in which your code was written, by your contract terms or by the laws in your country, could claim any rights (including but not limited to intellectual or industrial property) over your contributed code, you will have to send the project members (either by email from your authorised superiors or by signed fax), a statement indicating that your company agrees with the terms explained in this page, and that it both authorises your contribution to project and states that will never claim any kind of rights over it.