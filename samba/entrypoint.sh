#!/bin/bash
echo  "[INFO] HOSTNAME:$HOSTNAME, PUID:$PUID, PGID:$PGID, TZ:$TZ, BOOTSTRAP:'$BOOTSTRAP'"
if [ -f "$BOOTSTRAP" ]; then
   bash $BOOTSTRAP
else
   echo  "[ERROR] BOOTSTRAP:'$BOOTSTRAP' not found"
fi
