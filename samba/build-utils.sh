#!/bin/bash
#===========================================================
#===========================================================
#                myset/samba build utilities
#===========================================================
#===========================================================

# Login
docker login

# Check
docker image ls | grep myset/samba

# Build
docker build --rm -t myset/samba:latest .
docker build --rm -t myset/samba:202306 .

# Push
docker push myset/samba:latest
docker push myset/samba:202306

# Clean
docker rm myset/samba:latest
docker rm myset/samba:202306

# Pull
docker pull myset/samba:latest
docker pull myset/samba:202306

# Test
docker container stop test-samba
docker container rm test-samba
docker run -tid \
            --name test-samba \
            --hostname test-samba \
            -e PUID=1000 \
            -e PGID=1000 \
            -e TZ="Europe/Bucharest" \
            -e BOOTSTRAP="/SCRIPTS/test-samba.sh" \
            -v $HOME/SCRIPTS:/SCRIPTS \
            --cap-add=NET_ADMIN \
            myset/samba:latest