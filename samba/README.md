## General
Image contains [samba](https://www.samba.org/) standard Windows interoperability suite of programs for Linux and Unix. Samba has provided secure, stable and fast file and print services for all clients using the SMB/CIFS protocol, such as all versions of DOS and Windows, OS/2, Linux and many others.

This container can be used to launch any executable binary or script.


### Sample usage

Prepare SCRIPT folder and sample shell script
```
mkdir -p $HOME/SCRIPTS
vi $HOME/SCRIPTS/test-samba.sh
```
Sample shel script
```
#!/bin/bash
#======================================================
#======================================================
#                    test-samba
#======================================================
#======================================================

echo  "[INFO] test-samba started"

# Check samba version
echo "[INFO] samba $(samba --version)"

# Groups configuration
groupadd admins
groupadd testers

# Users configuration

# admin
smb_user="admin"
smb_pass="admin"
smb_group="admins"
useradd $smb_user
usermod -a -G $smb_group $smb_user
echo -e "$smb_pass\n$smb_pass" | smbpasswd -s -a "$smb_user"

# test1
smb_user="test1"
smb_pass="test1"
smb_group="testers"
useradd $smb_user
usermod -a -G $smb_group $smb_user
echo -e "$smb_pass\n$smb_pass" | smbpasswd -s -a "$smb_user"

# test2
smb_user="test2"
smb_pass="test2"
smb_group="testers"
useradd $smb_user
usermod -a -G $smb_group $smb_user
echo -e "$smb_pass\n$smb_pass" | smbpasswd -s -a "$smb_user"


# Samba configuration
cp /SCRIPTS/smb.conf /etc/samba/smb.conf
smbcontrol all reload-config


# Start services
service smbd start
service nmbd start

# Check
service smbd status
service nmbd status


echo  "[INFO] test-samba executed"

# Keep running
sleep infinity


```
Save and make it executable
```
chmod +x $HOME/SCRIPTS/test-samba.sh
```


Prepare samba configuration file smb.conf
```
vi $HOME/SCRIPTS/smb.conf
```
Sample smb.conf
```
# =====================================================================
# =====================================================================
#               Sample configuration file for Samba
# =====================================================================
# =====================================================================

# ---------------------------------------
#  global - common configuration
# ---------------------------------------
[global]
    workgroup = WORKGROUP
    server string = %h server
    server role = standalone server
    unix password sync = no
    pam password change = no
    map to guest = bad user
    usershare max shares = 100
    usershare allow guests = no
    # for old windows versions
    #server min protocol = NT1
    #lanman auth = yes
    # for secpol
    #ntlm auth = yes
    # tunning
    log level = 1
   
# ---------------------------------------
#  pub - shared folder
# ---------------------------------------
[pub]
	path = /STORAGE/pub
	comment = Public folder
	browseable = yes
	guest ok = no
	read only = no	
	writeable = yes
	valid users = @admins @testers
	write list = @admins @testers
	create mode = 0770
	directory mode = 0770
	force create mode = 0710
	force group = testers

# ---------------------------------------
#  priv - shared folder
# ---------------------------------------
[priv]
	path = /STORAGE/priv
	comment = Private folder
	browseable = yes
	guest ok = no
	read only = no	
	writeable = yes
	valid users = @admins
	write list = @admins
	create mode = 0770
	directory mode = 0770
	force create mode = 0710
	force group = admins


```


Prepare STORAGE folder for samba
```
mkdir -p $HOME/STORAGE
mkdir -p $HOME/STORAGE/pub
mkdir -p $HOME/STORAGE/priv
```


Create sample test-samba container

* BOOTSTRAP - Required environment variable contains full file path to the executable binary or script within container


```
docker run -tid \
            --name test-samba \
            --hostname test-samba \
            -e PUID=1000 \
            -e PGID=1000 \
            -e TZ="Europe/Bucharest" \
            -e BOOTSTRAP="/SCRIPTS/test-samba.sh" \
            -v $HOME/SCRIPTS:/SCRIPTS \
            -v $HOME/STORAGE:/STORAGE \
            myset/samba:latest
```
